#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import cgi
import cgitb

import postgresql


def delete():
    cgitb.enable()
    request = cgi.FieldStorage()
    db = postgresql.open('pq://andrey:password@localhost:5432/newdb')
    pdelete = db.prepare("DELETE FROM authors WHERE id = $1")
    pdelete(int(request.getvalue('id')))
    print("Content-Type: text/html;charset=utf-8\n\n")
    print('Deleted: %i' % (int(request.getvalue('id'))))


delete()

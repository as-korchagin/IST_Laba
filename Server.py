#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import postgresql


def connect():
    db = postgresql.open('pq://andrey:password@localhost:5432/newdb')
    response = db.query('SELECT id,name,description FROM authors')
    print("Content-Type: text/html;charset=utf-8\n\n")
    print('{0:3} | {1:20} | {2:70}'.format('id', 'name', 'description'), '<br>')
    print('____________________________________________________<br>')
    for i in response:
        print('{0:3} | {1:20} | {2:70}'.format(i[0], i[1], i[2]), '<br>')
        print('____________________________________________________<br>')
connect()

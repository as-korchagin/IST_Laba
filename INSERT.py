#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import cgi
import cgitb

import postgresql


def insert():
    cgitb.enable()
    request = cgi.FieldStorage()
    db = postgresql.open('pq://andrey:password@localhost:5432/newdb')
    ins = db.prepare("INSERT INTO authors(id,name,description) VALUES ($1, $2, $3)")
    ins(int(request.getvalue('id')), request.getvalue('name'), request.getvalue('description'))
    print("Content-Type: text/html;charset=utf-8\n\n")
    print('Inserted: %i, %s, %s' % (int(request.getvalue('id')), request.getvalue('name'), request.getvalue('description')))


insert()
